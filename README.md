# Terraform Elastic Cache Redis Module

## Contains

- Create a Redis Cache

## Outputs

- Redis primary endpoint

## How to use

### Setup Module
module "redis" {
  source                = "git@gitlab.com:terraform147/redis.git"
  availability_zones    = []
  name                  = ""
  number_cache_clusters = 4
  subnets               = []
  subnets_cidrs         = []
  vpc_id                = ""
}

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: