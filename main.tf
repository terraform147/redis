resource "aws_elasticache_subnet_group" "this" {
  name       = "${var.name}-subnet-group"
  subnet_ids = tolist(data.aws_subnet_ids.private.ids)
}

resource "aws_elasticache_replication_group" "this" {
  automatic_failover_enabled    = var.is_dev ? false : true
  availability_zones            = var.is_dev ? [var.availability_zones[0]] : var.availability_zones
  engine                        = "redis"
  node_type                     = var.node_type
  number_cache_clusters         = var.number_cache_clusters
  parameter_group_name          = "default.redis5.0"
  port                          = 6379
  replication_group_id          = "${var.name}-rep-group"
  replication_group_description = "${var.name} replication group"
  security_group_ids            = [aws_security_group.this.id]
  snapshot_window               = "03:00-06:00"
  snapshot_retention_limit      = 2
  subnet_group_name             = aws_elasticache_subnet_group.this.name
  at_rest_encryption_enabled    = var.enable_encryption
  transit_encryption_enabled    = var.enable_encryption
  tags = {
    Product = var.name
  }
}
