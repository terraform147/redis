resource "aws_security_group" "this" {
    name        = "${var.name}-redis-sg"
    description = "Allow TLS inbound traffic"
    vpc_id      = data.aws_vpc.selected.id

    egress {
      cidr_blocks = ["0.0.0.0/0"]
      description = "internet"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
    }

    tags = {
      Name = "${var.name}-redis-sg"
      Product = var.name
    }
}

resource "aws_security_group_rule" "subnet" {
  cidr_blocks       = data.aws_subnet.private.*.cidr_block
  description       = "redis-port"
  from_port         = 6379
  to_port           = 6379
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "bastion" {
  count             = var.bastion_cidr != "" ? 1 : 0
  cidr_blocks       = [var.bastion_cidr]
  description       = "bastion-redis-access"
  from_port         = 6379
  to_port           = 6379
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.this.id
}
