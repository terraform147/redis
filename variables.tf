variable "availability_zones" {
  type = list
}

variable "bastion_cidr" {
  default = ""
  type    = string
}

variable "enable_encryption" {
  type = bool
}

variable "is_dev" {
  description = "Defines the number of availability_zones used for the nodes"
  type        = bool
}

variable "name" {
  description = "Application name"
}

variable "number_cache_clusters" {
  description = "Amount of nodes on shard"
  default     = 2
}

variable "node_type" {
  description = "Elastic cache machine type"
  default     = "cache.t2.medium"
}

variable "vpc_id" {
  description = "VPC Id"
  type        = string
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

data "aws_subnet_ids" "private" {
  vpc_id = var.vpc_id

  tags = {
    Tier = "Private"
  }
}

data "aws_subnet" "private" {
  count = length(data.aws_subnet_ids.private.ids)
  id    = tolist(data.aws_subnet_ids.private.ids)[count.index]
}
